// Set click to be mouseup (like 'touchend') //
	var clickHandler = ("click");


//  START metrics functions, vars //	
	// boolean variables for flagging whether a metric has been set already for a session (for counting unique events only)
	var session, conclusion, timeout, userReset, techReset, endingSlides;
	session = conclusion = timeout = userReset = techReset = false;

	// create metrics key dictionary
	var metricKeys = {
		session:"start", 					 				
		conclusion:"finish",				
		timeout:"timeout",				
		userReset:"restart",
		techReset:"reset",
		lastSaved:"lastViewed",
		lastSavedObject:"lastViewedObj"
	};

	//var slideNames = ["screenSaver","T2DToday","efficacyUnrealized","adherance","adherance2","hospitalization","conclusion"];
	localStorage.setItem(metricKeys.lastSaved,0);

	var slideNamesArr = {};

	if (! localStorage.getItem(metricKeys.lastSavedObject)) {
		 slideNamesArr = [
			{
				page_id: "screenSaver",
				ended_sessions: "0"
			},
			{
				page_id: "T2DToday",
				ended_sessions: "0"
			},
			{
				page_id: "efficacyUnrealized",
				ended_sessions: "0"
			},
			{
				page_id: "adherance",
				ended_sessions: "0"
			},
			{
				page_id: "adherance2",
				ended_sessions: "0"
			},
			{
				page_id: "hospitalization",
				ended_sessions: "0"
			},
			{
				page_id: "conclusion",
				ended_sessions: "0"
			}
		];
			//alert(slideNamesArr[1].page_id);
	} else {
		slideNamesArr = JSON.parse(localStorage.getItem(metricKeys.lastSavedObject));
	}

function storeMetric(key, count) {
  localStorage.setItem(key,count);
}
function incrementEndingSlideCount(index){
   if (index==0) return true;
   var count = parseInt(slideNamesArr[index].ended_sessions);
   count++;
   console.log('new count: ' + count.toString());
   slideNamesArr[index].ended_sessions = count.toString();

   console.log('saving obj: ' + JSON.stringify(slideNamesArr))
   localStorage.setItem(metricKeys.lastSavedObject, JSON.stringify(slideNamesArr));
}
//  END metrics functions, vars //	

// Refresh the page (Fail Safe) //
$("#flag").bind(clickHandler, function() {
    //track failsafe reset by technician or power user
    if (!techReset) {
    	var techresets = (localStorage.getItem(metricKeys.techReset)===null) ? 0 : localStorage.getItem(metricKeys.techReset);
		techresets++;
		storeMetric(metricKeys.techReset, techresets);
		//console.log('updated techReset to ' + techresets);
		techReset = true;

		// update metric for last viewed slide (ending slide)
		incrementEndingSlideCount(localStorage.getItem(metricKeys.lastSaved));
	}
	//reload page
	location.reload();

	// reset metric state flags
	//session = conclusion = timeout = userReset = techReset = false;
});

// New Stick Figure Death Function (Still a sweet band name)
var counter = 0;//set this to what ever you want the start # to be
	countUP();//call the function once	
	function countUP() {
		if (counter == 7) {
			counter = 1;
			$("#stickLife li").removeClass("dead");
			}
		else {
			$("#stickLife li:nth-child("+counter+")").addClass("dead");
				counter++;//increment the counter by 1
			}
		setTimeout ( "countUP()", 6000 );
	}

// Check if user is active //
function idle() {
	var idleState = false;
	var idleTimer = null;
	// Bind a bunch of mouse/touch events to everything to check if a user is just sitting there //
	$('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
		clearTimeout(idleTimer);
		if (idleState === true) {
			// Don't do anything if a user is active
			}
		idleState = false;
		// Start the 30 second timer as soon as a user stops interacting with the screen, as long as the user is not on the screen saver //
		if ($(".one").hasClass("notSS")){
			idleTimer = setTimeout(function () {          
			idleState = true;   
			$("#celophane").addClass("blur");
			$("#modalBG").fadeIn(300);
			$("#modal").fadeIn(300);
			$("#modal div").hide();
			$("#timeOut").show();						
			}, 30000);
			//console.log('idle');
		} else {
		// User is on the screen saver, so don't execute the 2 minute timer //					
			clearTimeout(idleTimer);
			idle = function(){};
			}
		}
	);
	$(".two, .three, .four, .five, .six, .seven").trigger("mousemove");
}

// Check if user has abandoned the panel after 2 minutes and 30 seconds //
function resetPanel() {
	var resetState = false;
	var resetTimer = null;
	// Bind a bunch of mouse/touch events to everything to check if a user is just sitting there //
	$('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
		clearTimeout(resetTimer);
		if (resetState === true) {
			// Don't do anything if a user is active
		}
		resetState = false;
	// Start the 45 second timer as soon as a user stops interacting with the screen, as long as the user is not on the screen saver //
		if ($(".one").hasClass("notSS")){
		resetTimer = setTimeout(function () {
				$(".one").removeClass("notSS");
				$("#celophane").removeClass("blur");
				$("#modalBG").fadeOut(300);
				$("#modal").fadeOut(300);
				$("#modaldiv, #ticker").fadeOut(300);

			    //track automatic timeout
			    //if (!timeout){
			    var timeoutCount = (localStorage.getItem(metricKeys.timeout)===null) ? 0 : localStorage.getItem(metricKeys.timeout);
				timeoutCount++;
				storeMetric(metricKeys.timeout, timeoutCount);
				//timeout = true;
				//console.log('updated timeout to ' + timeoutCount);
				//}

				incrementEndingSlideCount(localStorage.getItem(metricKeys.lastSaved));

				// reset metric state flags
				session = conclusion = timeout = userReset = techReset = false;

			   $("#swipe").dragend({
				scrollToPage: 1
				  });
				}, 45000);
			//console.log('reset');
	} else {
		// User is on the screen saver, so don't execute the 2 minute timer //					
		clearTimeout(resetTimer);
		resetPanel = function(){};
			}
		}
	);
	$(".two, .three, .four, .five, .six, .seven").trigger("mousemove");
}		

// Sliders //
$(function() {
  $("#swipe").dragend({	
	onDrag:	 function(){
		// Add temporary class to determine that the user is swiping (so we can stop click event from firing later) //
		$("#swipe").addClass("swiping");
		},
	onSwipeStart: function() {
		
  if (this.page === 0) {
		 
		// Set a class token to state that the user is not on the screen saver //
		$(".one").addClass("notSS");
		 idle();	
		 resetPanel();		  
	  } 
	if (this.page === 1) {
		// Set a class token to state that the user now on the screen saver //
		$(".one").removeClass("notSS");
		}
	}, 
	onSwipeEnd: function() {
		// Remove temporary class that determines if the user is swiping //
		$("#swipe").removeClass("swiping");
	  if (this.page !== 0) {
			// Show the circle indicator on all pages but the screen saver //		  
		  $(".nav").show();
		  $(".one").addClass("off");
		  }
	  else {
		// Full-on Reset //
		  	location.reload();	
		  }
	if (this.page === 2) {
		// Re-set a class token to state that the user not on the screen saver //
		$(".one").addClass("notSS");
		// Start the Chart 1 Animation //		  
		$("#chart1 div, #chart1 img").addClass("show");
		} else {
		$("#chart1 div, #chart1 img").removeClass("show");
		if (this.page === 6) {
			// Start the Chart 2 Animation //		  
			setTimeout(function(){$("#chart2 div, #chart2 img").addClass("go");}, 500);
			// Add a class token to state that the user is on screen 7 //
			$(".seven").addClass("pSeven");
		} else {
			// Re-set a class token to state that the user is not on screen 7 //
			$(".seven").removeClass("pSeven");
			$("#chart2 div, #chart2 img").removeClass("go");

			if (this.page === 7){
				//track Conclusion pg view
				    if (!conclusion) {
				    	var conclusionCount = (localStorage.getItem(metricKeys.conclusion)==null) ? 0 : localStorage.getItem(metricKeys.conclusion);
						conclusionCount++;
						storeMetric(metricKeys.conclusion, conclusionCount);
						//console.log('updated conclusion count to ' + conclusionCount);
						conclusion = true;
					}
			}
		}
	}
	  // Controls which dot is highlighted in the circle indicator //
	  $(".nav li").removeClass("active");
	  $(".nav li").eq(this.page).addClass("active");
	//console.log('dragged to ' + this.page);

	  console.log('last viewed index: ' + this.page);
	  storeMetric(metricKeys.lastSaved, this.page);
	}

});
// End Sliders //

// Tap or Click anywhere on the screen saver page //
$("#screenSaver").bind(clickHandler, function() {
	if ($("#swipe").hasClass("swiping")){
		// Don't continue if if a user is swiping //
	    return false;			
		}

		// Set a class token to state that the user is not on the screen saver //
		$(".one").addClass("notSS");

		// Check for Idle Users //
		 idle();	
		 resetPanel();	
		 	  
		// Animate to the next slide //
	   $("#swipe").dragend({
			scrollToPage: 2
		  });
	   if (!session) {
	    	var starts = (localStorage.getItem(metricKeys.session)==null) ? 0 : localStorage.getItem(metricKeys.session);
			starts++;
			storeMetric(metricKeys.session, starts);
			session = true;
			//console.log('updated session to ' + starts);
		}
		
	});		  

$("#T2DToday .next").bind(clickHandler, function(){
	if ($("#swipe").hasClass("swiping")){
		// Don't continue if if a user is swiping //
	    return false;			
		}
	// Animate to the next slide //
   $("#swipe").dragend({
	scrollToPage: 3
	  });
  });		  

$("#efficacyUnrealized .next").bind(clickHandler, function() {
	if ($("#swipe").hasClass("swiping")){
		// Don't continue if if a user is swiping //
	    return false;			
		}
	// Animate to the next slide //
   $("#swipe").dragend({
	scrollToPage: 4
	  });
	});	
		  
$("#adherance .next").bind(clickHandler, function() {
	if ($("#swipe").hasClass("swiping")){
		// Don't continue if if a user is swiping //
	    return false;			
		}
	// Animate to the next slide //
   $("#swipe").dragend({
	scrollToPage: 5
	  });
	});	

$("#adherance2 .next").bind(clickHandler, function() {
	if ($("#swipe").hasClass("swiping")){
		// Don't continue if if a user is swiping //
	    return false;			
		}
	// Animate to the next slide //
   $("#swipe").dragend({
	scrollToPage: 6
	  });
	});	

// Button for moving to the last slide //  
$("#hospitalization .next").bind(clickHandler, function() {
	if ($("#swipe").hasClass("swiping")){
		// Don't continue if if a user is swiping //
	    return false;			
		}
	// Animate to the next slide //
   $("#swipe").dragend({
	scrollToPage: 7
	  });
});

// User resets the panel by choosing 'no' on the time out modal //		  
$("#no").bind(clickHandler, function() {
	//console.log("got it");
	// Reset the panel (all tickers, timers, etc.) and moves the user to the screen saver page //
	$(".one").removeClass("notSS");
	$("#celophane").removeClass("blur");
	$("#modalBG").fadeOut(300);
	$("#modal, #ticker").fadeOut(300);
	$("#modaldiv").fadeOut(300);

	//if (!userReset) {
		var userresets = (localStorage.getItem(metricKeys.userReset)==null) ? 0 : localStorage.getItem(metricKeys.userReset);
		userresets++;
		storeMetric(metricKeys.userReset, userresets);
		//console.log('updated userReset to ' + userresets);
		//userReset = true;
	//}

		incrementEndingSlideCount(localStorage.getItem(metricKeys.lastSaved));

	// reset all metric state flags, since user has explicitly asked to start over
	session = conclusion = timeout = userReset = techReset = false;

	   $("#swipe").dragend({
		scrollToPage: 1
		  });
	});

$("#modalBG, #yes, .closeX").bind(clickHandler, function() {
	// Close the Modal and reset any content in the modals //
	$("#celophane").removeClass("blur");
	$("#modalBG").fadeOut(300);
	$("#modal").fadeOut(300);
	$("#modaldiv").fadeOut(300);
	setTimeout(function(){
		$("#gc .buttonSmall").removeClass("on");
	 	$("#gc .buttonSmall img").attr("src", "img/nav/buttonSmallArrow.png");
 },300);
});
	
$( document ).ready(function() {
	setTimeout(function(){
		$("#wrapper").addClass("show");
	  },1);


// Open the Modal Window //
$(".openModal").bind(clickHandler, function() {
	if ($("#swipe").hasClass("swiping")){
		// Don't continue if if a user is swiping //
					return false;
					}
		$("#celophane").addClass("blur");
		$("#modalBG").fadeIn(300);
		$("#modal").fadeIn(300);
		$("#modal div").hide();
		
		});


// Ticker Logic //
	// Set time of Convention Start //
	if (moment() >= moment([2016, 8, 21, 16.5])) {
		var conventionStart = moment([2016, 8, 21, 16.5]); // Actual Convention Start
		}	
	else {
		var conventionStart = moment([2016, 8, 20, 16.5]); // "Convention Start" for testing
		}
	
	//	Set variables for continuous deaths every 6 seconds from start of convention //
	var now = moment();
	var startConventionToNow = parseInt((now.diff(conventionStart)/1000).toFixed(0));
	var deathsSinceConventionStart = (startConventionToNow/6).toFixed(0);
	var ticker = setTimeout(SixSeconds, 6000);
	var lives = parseInt(deathsSinceConventionStart);

	//	Continually count up a new death every 6 seconds //	
	function SixSeconds() {
		  $("#lives").html((lives+1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
		
	// Update the lives lost since start of convention on the #livesLost screen //
	  $("#lives2 .deaths").html((lives+1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));	  
	
	// Update the top ticker lives lost since start of convention //
	  lives = lives+1;
	  setTimeout(function(){	 
	 	 	$("#ticker").addClass("none").removeClass("death");
		  }, 1000);
	  ticker = setTimeout(SixSeconds, 6000); // repeat
	}
	
//	Set timer to count up from start of convention //
	Date.prototype.addSeconds= function(h){
		this.setSeconds(this.getSeconds()+h);
		return this;
	}
	var date = new Date().setSeconds(-startConventionToNow) / 1000
	
//	Return the timer //
	$('.timer2').countid({
		clock: true,
		dateTime: date,
		dateTplElapsed: "%H:%M:%S"
	})

//	Print the lives lost since start of convention //
	$("#lives2 .deaths").html(lives.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	});
});

