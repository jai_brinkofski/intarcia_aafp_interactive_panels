// Prevent click events from interfering with touch events //
$(document).on('touchstart', function(event){
	event.target.click();
	event.preventDefault();
});

// Allow for both touch and click events for desktop/touch-enabled device use //
var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

// StickLives //
function stickLives(stopOrGo) {
var stickLives = '#stickLives';
var li = '#stickLives li';
var $li = $(li);

console.log(stopOrGo);

if(stopOrGo == "go") {
    $li.each(function (i) {
        var timer = window.setTimeout(function () {
            $li.eq(i).stop().addClass("dead");
        }, 6000 * (i + 1));

        $(this).data('timerId', timer); //save timer id here
		
		
})
	livesSinceStart = 0;	
	function livesPanel() {
		$("#livesSinceStart").html(livesSinceStart);
		  livesSinceStart = livesSinceStart+1;
		  livesPanelTicker = setTimeout(livesPanel, 6000); // repeat
        $(this).data('timerId2', livesPanelTicker); //save timer id here
	}
	livesPanel();
}

else { 

            $li.each(function () {
            window.clearTimeout($(this).data('timerId')); //clear it here
			timer=function(){};
        });
		    window.clearTimeout($(this).data('timerId2')); //clear it here
			livesPanel=function(){};
			$("#livesSinceStart").html(0);
        $li.stop(true, true, true).removeClass("dead");

}

return stopOrGo;


}


// Check if user is active //
function idle() {
			var idleState = false;
			var idleTimer = null;
			$('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
				clearTimeout(idleTimer);
				if (idleState == true) {
					console.log('stop being idle');
				}
				idleState = false;
				if ($(".one").hasClass("notSS")){
				idleTimer = setTimeout(function () {          
					idleState = true;   
					$("#celophane").addClass("blur");
					$("#modalBG").fadeIn(300);
					$("#modal").fadeIn(300);
					$("#modal div").hide();
					$("#timeOut").show();						
						console.log('start being idle') 
					}, 120000);
			} else {
				clearTimeout(idleTimer);
				idle = function(){};
				}
				}
			
			);
			$(".two, .three, .four, .five").trigger("mousemove");
		
		}
		

function resetPanel() {
			var resetState = false;
			var resetTimer = null;
			$('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
				clearTimeout(resetTimer);
				if (resetState == true) {
					console.log('stop being reset');
				}
				resetState = false;
				if ($(".one").hasClass("notSS")){
				resetTimer = setTimeout(function () {
						$(".one").removeClass("notSS");
						$("#celophane").removeClass("blur");
						$("#modalBG").fadeOut(300);
						$("#modal").fadeOut(300);
						$("#modaldiv, #ticker").fadeOut(300);
					   $("#swipe").dragend({
						scrollToPage: 1
						  });
						console.log("reset");
						}, 150000);
			} else {
				clearTimeout(resetTimer);
				resetPanel = function(){};
				}
				}
			
			);
			$(".two, .three, .four, .five").trigger("mousemove");
		
		}

		
					

		


// Sliders //
    $(function() {
		
		
      $("#swipe").dragend({		 
        onSwipeStart: function(e) {
			console.log("swipe");
			$("#swipe").addClass("swiping");
	  if (this.page == 0) {
			  var stopOrGo = "go";
			  stickLives(stopOrGo);
			$(".one").addClass("notSS");
			$("#ticker").fadeIn(250);			
   			 idle();	
			 resetPanel();		  
		  } 
	if (this.page == 1) {
			$(".one").removeClass("notSS");
			}
        }, 
        onSwipeEnd: function() {
			$("#swipe").removeClass("swiping");
		  if (this.page != 0) {
			  $(".nav").show();
			  }
		  else {
			  $(".nav").fadeOut(100);
			  $(".dead").removeClass("dead");
			  detached = $("#stickLives").detach();
			  detached.appendTo("#lifeLoss");
			  $("#ticker").fadeOut(250);			
			  var stopOrGo = "stop";
    		  stickLives(stopOrGo);
			  }
		if (this.page == 2) {
			$(".one").addClass("notSS");
			}

          var first = this.pages[0],
              last = this.pages[this.pages.length - 1];
          $(".nav li").removeClass("active");
          $(".nav li").eq(this.page).addClass("active");
        }
		
      });



		$("#screenSaver").bind(clickHandler, function(e) {
			if ($("#swipe").hasClass("swiping")){
					return false;
					}
			  var stopOrGo = "go";
			$(".one").addClass("notSS");
		   $("#swipe").dragend({
			scrollToPage: 2
			  });
			$(".one").addClass("notSS");
			$("#ticker").fadeIn(250);			
   			 idle();
			 stickLives(stopOrGo);
		  });		  

		$("#T2DToday .next").bind(clickHandler, function(e){
			if ($("#swipe").hasClass("swiping")){
					return false;
					}
		   $("#swipe").dragend({
			scrollToPage: 3
			  });
		  });		  

		$("#efficacyUnrealized .next").bind(clickHandler, function(e) {
if ($("#swipe").hasClass("swiping")){
					return false;
					}
		   $("#swipe").dragend({
			scrollToPage: 4
			  });
		  });	
		  
		$("#adherance .next").bind(clickHandler, function(e) {
if ($("#swipe").hasClass("swiping")){
					return false;
					}
		   $("#swipe").dragend({
			scrollToPage: 5
			  });
		  });	
		  
		$("#no").bind(clickHandler, function(e) {
			$(".one").removeClass("notSS");
			$("#celophane").removeClass("blur");
			$("#modalBG").fadeOut(300);
			$("#modal, #ticker").fadeOut(300);
			$("#modaldiv").fadeOut(300);
			   $("#swipe").dragend({
				scrollToPage: 1
				  });
			});

});

	$("#modalBG, #yes, .closeX").bind(clickHandler, function(e) {
		$("#celophane").removeClass("blur");
		$("#modalBG").fadeOut(300);
		$("#modal").fadeOut(300);
		$("#modaldiv").fadeOut(300);
		setTimeout(function(){
			$("#gc .buttonSmall").removeClass("on");
      },300);
	});
	




$( document ).ready(function() {
	$(".hba1cTab").bind(clickHandler, function(e) {
			$(".hba1cModal").attr("src", "img/hba1cModal.png");
		});
	$(".mortalityTab").bind(clickHandler, function(e) {
			$(".hba1cModal").attr("src", "img/modals/mortalityModal.png");
		});
	
	$(".glpTab").bind(clickHandler, function(e) {
			$(".glpModal").attr("src", "img/modals/glpModal.png");
		});
	$(".dppTab").bind(clickHandler, function(e) {
			$(".glpModal").attr("src", "img/modals/dppModal.png");
		});

	$("#gc .buttonSmall").bind(clickHandler, function(e) {
		$("#gc .buttonSmall").removeClass("on");
		$("#gc .buttomSmall img").attr("src", "img/nav/buttonSmallArrow.png");
		$(this).addClass("on");
		$("img", this).attr("src", "img/nav/upArrow.png");
		if ($(this).hasClass("gc")){
			$(".gcModal").attr("src", "img/modals/gcModal.png");
			$("#gc .complications img").attr("src", "img/nav/buttonSmallArrow.png");
			}
		else {
			$("#gc .gc img").attr("src", "img/nav/buttonSmallArrow.png");
			$(".gcModal").attr("src", "img/modals/gcModal2.png");		
			}
		});

	$(".openModal").bind(clickHandler, function(e) {
if ($("#swipe").hasClass("swiping")){
					return false;
					}
		$("#celophane").addClass("blur");
		$("#modalBG").fadeIn(300);
		$("#modal").fadeIn(300);
		$("#modal div").hide();
		if ($(this).hasClass("a1cGoal")) {
			$("#a1cGoal").show();
				}
		if ($(this).hasClass("disconnect")) {
			$(".glpModal").attr("src", "img/modals/glpModal.png");
			$("#disconnect").show();
				}
		if ($(this).hasClass("gc")) {
			$("#gc").show();
			$("#gc .gc").addClass("on");
			$("#gc .gc img").attr("src", "img/nav/upArrow.png");
			$(".gcModal").attr("src", "img/modals/gcModal.png");
				}
		if ($(this).hasClass("complications")) {
			$("#gc").show();
			$("#gc .complications").addClass("on");
			$("#gc .complications img").attr("src", "img/nav/upArrow.png");
			$(".gcModal").attr("src", "img/modals/gcModal2.png");
				}
		if ($(this).hasClass("fifty")) {
			$("#fifty").show();
				}
		if ($(this).hasClass("twentyfive")) {
			$("#twentyfive").show();
				}
		if ($(this).hasClass("hba1c")) {
			$("#hba1c").show();
			$(".hba1cModal").attr("src", "img/modals/hba1cModal.png");
				}
		if ($(this).hasClass("mortality")) {
			$("#hba1c").show();
			$(".hba1cModal").attr("src", "img/modals/mortalityModal.png");
				}
		});


	// Ticker Logic //
	if (moment() >= moment([2016, 5, 11, 10])) {
		var conventionStart = moment([2016, 5, 11, 10]);
		}	
	else {
		var conventionStart = moment([2016, 4, 9, 0]);
		}
	var now = moment();
	var startConventionToNow = parseInt((now.diff(conventionStart)/1000).toFixed(0));
	var deathsSinceConventionStart = (startConventionToNow/6).toFixed(0);
	var ticker = setTimeout(SixSeconds, 6000);
	var lives = parseInt(deathsSinceConventionStart);
	function SixSeconds() {
		if (lives < 10) {
		  $("#lives").html("000"+(lives+1));
		}
		else if (lives < 100 && lives >=10) {
		  $("#lives").html("00"+(lives+1));
		}
		else if (lives < 1000 && lives >=100) {
		  $("#lives").html("0"+(lives+1));
		}
		else {
		  $("#lives").html(lives+1);
		}
	  $("#lives2 .deaths").html((lives+1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));	  
	  $("#ticker").addClass("death").removeClass("none");
	  lives = lives+1;
	  setTimeout(function(){
	 	 $("#ticker").addClass("none").removeClass("death");
		  }, 1000);
	  ticker = setTimeout(SixSeconds, 6000); // repeat
	}
	Date.prototype.addSeconds= function(h){
		this.setSeconds(this.getSeconds()+h);
		return this;
	}
	var date = new Date().setSeconds(-startConventionToNow) / 1000
	
	
	$('.timer2').countid({
		clock: true,
		dateTime: date,
		dateTplElapsed: "%H:%M:%S"
	})

$("#lives, #lives2 .deaths").html(lives.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

});
