// create and append link to an actual CSV file for download
function createExportLink(data) {
    var exportLink = document.createElement('a');
    exportLink.setAttribute('href', 'data:text/csv;base64,' + window.btoa(data));
    exportLink.appendChild(document.createTextNode('Export to download.csv'));
    document.getElementById('metricsDisplay').appendChild(exportLink);
}

// use displayed table to generate CSV data
function prepExportData(){
    var data = '';
    var colSep = '';
    var rowSep = '\r\n';

    //prep CSV header row
    $("th").each(function() {
      data += colSep + $(this).text();
      colSep = ',';
    });
    data += rowSep;

    // prep CSV rows for simple key-values
    $("tbody > tr.val").each(function() {

          colSep = '';
      $( this ).children("td").each(function() {
        data += colSep + $(this).text();
        colSep = ',';
      });
      data += rowSep;

    });

    // prep CSV rows for JSON objects that were displayed differently (nested) in HTML table
    $("tbody > tr.obj").each(function() {

      var $data_obj_element = $( this ).children("td").eq(1);
      var prefix = $data_obj_element.attr("data-metric-prefix");

      $data_obj_element.children("div").each(function() {
        data += prefix + $(this).children("span.objKey").text();
        data += ',';
        data += $(this).children("span.objVal").text();

        data += rowSep;
      });


    });

    //console.log(data);
    return data;
}

/* main script */
$(document).ready(function() {


  // display simple metric key-values
  $(".metricval").each(function() {
    var metric = (localStorage.getItem($(this).attr('data-metric'))==null) ? 0 : localStorage.getItem($(this).attr('data-metric'));
    $( this ).text(metric);
  });

  // flatten any metric JSON objects and display members as key-values
  $(".metricobj").each(function() {
    var metric = (localStorage.getItem($(this).attr('data-metric'))==null) ? {} : JSON.parse(localStorage.getItem($(this).attr('data-metric')));
    var obj_data_html = "";

    for (var key in metric) {
      if (metric.hasOwnProperty(key)) {
        //alert(key + " -> " + metric[key].page_id);
            obj_data_html += '<div class="objRow"><span class="objKey">' + metric[key].page_id.toString() + '</span>: <span class="objVal">' + metric[key].ended_sessions.toString() + '</span></div><br>';

      }
    }
    $( this ).html(obj_data_html);

  });


  var metricData = prepExportData();
  createExportLink(metricData);

});
